from sys import argv

script, filename = argv

print "I'm going to open %r file now." % filename
print "I'm going to delete the contents. If you dont want it hit CTRL + C."
print "If you want that hit RETURN"

raw_input("? ")

print "Opening file..."
target = open(filename, 'w')

print "Truncating the file...Goodbye!!!"
target.truncate()

print "Now I'm going to as you for 3 lines:"
line1 = raw_input("line 1: ")
line2 = raw_input("line 2: ")
line3 = raw_input("line 3: ")

print "I'm going to write these files to the file %r." % filename

target.write(line1)
target.write("\n")
target.write(line2)
target.write("\n")
target.write(line3)
target.write("\n")

print "And finally we close it..."

target.close()

print "New contents in the file would be:"
target = open(filename)
print target.read()