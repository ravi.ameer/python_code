total_employees = 800
total_pantries = 8
total_coffee_machines = total_pantries * 3
total_coffee_cups = total_coffee_machines * 100
employee_coffee_times = 10
cups_needed = total_employees * employee_coffee_times
cups_shortage = cups_needed - total_coffee_cups

print "There are total of", total_employees ,"employees in office."
print "There are total", total_pantries ,"pantries in office"
print "There are total", total_coffee_machines ,"coffee machines and", total_coffee_cups ,"coffee cups in office"
print "An employee will take appx.", employee_coffee_times ," coffee a day"
print "In this context, actual cups needed would be", cups_needed ,"per day"
print "We are running short of", cups_shortage ,"cups"
print "In short, we are screwed!!!"