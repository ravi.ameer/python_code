from sys import argv
script, filename = argv #should give filename as an argument at the end of script
txt = open(filename) #txt is a function to read the file given as an o/p at the start of the script
print "Here is your file %r:" % filename #print the filename provided at the start
print txt.read() #printing the filename with the help of function at line 3
print "Type the file name again:" #prompt the user again for the file input
file_again = raw_input ("> ") #catch the o/p using raw_input starting with fancy prompt >
txt_again = open(file_again) #once again writing function to open the file we got at line 7
print txt_again.read() #same as line 5