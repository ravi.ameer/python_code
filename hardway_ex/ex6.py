x = "There are %d types of people." % 10 #declaring %d as 10
binary = "binary" #declaring a string
do_not = "don't" #same as above step
y = "Those who knows %s and those who %s" % (binary , do_not) #displaying 2 strings
print x #displaying data in x
print y #displaying data in y
print ("I said: %r.") % x #printing recursive output
print ("I also said: %r.") % y #printing recursive output
hilarious = False #declating string as decision
joke_evaluation = "Isn't that joke so funny?! %r" #adding blank recursive for future purpose
print joke_evaluation % hilarious #printing above statements along with recursive dummy
w = "This is a left side of..." #string 1
e = "a string with a right side." #string 2
print w + e #adding and printing both of them